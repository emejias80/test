<?php

namespace App\Http\Controllers;

use App\Data;
use Illuminate\Http\Request;
use Storage;

class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Storage::disk('local')->exists('product.json')){
            $json = Storage::disk('local')->get('product.json');
            $json = json_decode($json, true);

            foreach ($json as &$key) {
                $key['total']  = $key['qty'] * $key['price'];
            }

            $datetime = array_column($json, 'datetime');
            array_multisort($datetime, SORT_ASC, $json);

            $total = array_sum(array_column($json,'total'));

        } else {
           $json = array();
           $total = 0;
        }

        return view ('index')->withData($json)->withTotal($total);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         try {
            $jsonFile = Storage::disk('local')->exists('product.json') ? json_decode(Storage::disk('local')->get('product.json')) : [];
            $inputData = $request->only(['product', 'qty', 'price']);
            $inputData['datetime'] = date('Y-m-d H:i:s');
            array_push($jsonFile, $inputData);
            Storage::disk('local')->put('product.json', json_encode($jsonFile));
            $inputData['total'] = $inputData['qty'] * $inputData['price'];
            return ['error' => false, 'message' => 'Register Saved', 'data' => $inputData];
 
        } catch(Exception $e) {
 
            return ['error' => true, 'message' => $e->getMessage()];
 
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Data  $data
     * @return \Illuminate\Http\Response
     */
    public function show(Data $data)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Data  $data
     * @return \Illuminate\Http\Response
     */
    public function edit(Data $data)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Data  $data
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Data $data)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Data  $data
     * @return \Illuminate\Http\Response
     */
    public function destroy(Data $data)
    {
        //
    }
}
