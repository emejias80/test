<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>@yield('title','Developer Test')</title>
	<script type="text/javascript" src="{{ asset('plugins/jquery/jquery-3.3.1.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>
	@yield('jsfiles')
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}">
	@yield('styles')
</head>
<body>
<section>
	@yield('content')
</section>
</body>
@yield('endjs')
</html>