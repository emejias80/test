@extends('template.main')

@section('jsfiles')
@endsection

@section('styles')
	<style>

	  body {
	    margin: 40px 10px;
	    padding: 0;
	    font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
	    font-size: 14px;
	  }

	  #form {
	    max-width: 700px;
	    margin: 0 auto;
	  }

	  #divTable {
	    max-width: 900px;
	    margin: 25px auto;
	  }
	</style>
@endsection

@section('content')
<form id="product-form" action="{{url('/data/store')}}" method="POST"> 
	<div id="form">
		<div class="form-group">
			<label>Product Name</label>
			<input type="text" class="form-control" name="product" id="product" placeholder="Enter Product Name">
		</div>
		<div class="row">
			<div class="form-group col-6">
				<label>Quantity in Stock</label>
				<input type="number" class="form-control" name="qty" id="qty" placeholder="Quantity">
			</div>
			<div class="form-group col-6">
				<label>Price per Item</label>
				<input type="number" class="form-control" name="price" id="price" placeholder="Price per Item">
			</div>
		</div>
		<button type="button" class="btn btn-primary" id="saveData">Submit</button>
	</div>
</form>

<div class="table-responsive text-center" id="divTable">
    <table class="table table-borderless" id="table">
        <thead>
            <tr>
                <th class="text-center">Product Name</th>
                <th class="text-center">Quantity in Stock</th>
                <th class="text-center">Price per Item</th>
                <th class="text-center">Datetime Submitted</th>
                <th class="text-center">Total Value Number</th>
            </tr>
        </thead>
        <tbody>
	        @foreach($data as $item)
	        <tr>
	            <td align="left">{{ $item['product'] }}</td>
	            <td>{{ $item['qty'] }}</td>
	            <td align="right">{{ $item['price'] }}</td>
	            <td>{{ $item['datetime'] }}</td>
	            <td align="right">{{ $item['total'] }}</td>
	        </tr>
	        @endforeach
    	</tbody>
		<tfoot>
			<tr>
				<td align="right" colspan="4">Total value numbers</td>
				<td align="right" id="totalTable">{{ $total }}</td>
			</tr>
		</tfoot>
    </table>
</div>
@endsection

@section('endjs')
<script type="text/javascript">
	$('#saveData').click(function(){
		$.ajax({
			url: '{{ url('/data/store') }}',
			headers: {
			  'X-CSRF-TOKEN': '{{ csrf_token() }}'
			},
			method: 'POST',
			data: $('#product-form').serialize(),
			success: function(res) {
				alert(res.message);
				$("#product-form")[0].reset();
				html =	'<tr>'+
						'<td align="left">' + res.data.product + '</td>' +
						'<td>' + res.data.qty + '</td>' +
						'<td align="right">' + res.data.price + '</td>' +
						'<td>' + res.data.datetime + '</td>' +
						'<td align="right">' + res.data.total + '</td>' +
						'</tr>';

				$("#table").append(html);
				$("#totalTable").text(Number($("#totalTable").text()) + res.data.total);

				console.log(res);
			},
			error: function (res) {
				console.log(res);
			}
		});
	});
</script>
@endsection